from flask import Flask, jsonify
from picamera import PiCamera
from time import sleep
import os

app = Flask(__name__)

@app.route("/app")
def index():
  return "<html><body><h1>Test2 site running under Flask</h1></body></html>"

@app.route("/camera")
def camera():
  return jsonify({'data': {'response': 'DONE', 'content': 'BLALBLA'}})

@app.route("/startpicture")
def startpicture():

  # starting camera is crashing!!!
  #camera = PiCamera()
  #camera.start_preview()
  #sleep(5)
  #camera.capture('/var/www/html/images/picamera-image.jpg')
  #camera.stop_preview()
  #with open('/home/pi/timelapse.on', 'rw') as f:
  #  f.write('hellow ww')

  # Write a file to enable timelapse.
  file = open('/tmp/timelapse.on', 'w+')
  return 'start'

@app.route("/stoppicture")
def stoppicture():
  # Remove the file to disable timelapse.
  os.remove('/tmp/timelapse.on')
  return 'stop'

if __name__ == "__main__":
  app.run(host='0.0.0.0',debug=True)
